# Pandoc LaTeX Templates
These are my personal pandoc LateX templates that I use for my documents,
letters, presentations and CV.

I use these in combination with my [docker image](https://gitlab.com/severinkaderli/pandoc-docker)
to compile my documents using the GitLab CI.

## How to use
### Continuous integration
1. Add this repository as a submodule  
```bash
git submodule add https://gitlab.com/severinkaderli/pandoc-latex-templates .latex
```

2. Add the following code to `.gitlab-ci.yml`
```yaml
include:
  - project: 'severinkaderli/pandoc-latex-templates'
    file: '/templates/.gitlab-ci.yml'

variables:
  PANDOC_FILES: >
    ./Notes/Lecture_Notes.md
```

3. Add the files you wish to compile to the `PANDOC_FILES` variable.

## Samples
* [Document](https://gitlab.com/severinkaderli/pandoc-latex-templates/builds/artifacts/master/raw/document.pdf?job=PDF)
* [Presentation](https://gitlab.com/severinkaderli/pandoc-latex-templates/builds/artifacts/master/raw/beamer.pdf?job=PDF)
* [Letter](https://gitlab.com/severinkaderli/pandoc-latex-templates/builds/artifacts/master/raw/letter.pdf?job=PDF)


# Variables
## Document
| Variable                    | Description                       | Default Value |
| --------------------------- | --------------------------------- | ------------- |
| image-width                 | Width of images                   | `0.8`         |
| note-background-color       | Background color for note content | `e7f2fa`      |
| note-title-background-color | Background color for note title   | `6ab0de`      |

## Cheatsheet
| Variable    | Description     | Default Value |
| ----------- | --------------- | ------------- |
| image-width | Width of images | `0.5`         |
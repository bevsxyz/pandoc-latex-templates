#!/bin/bash
# 
# SCRIPT NAME:
# build.sh
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# This script converts a markdown file to PDF using pandoc.
#
# USAGE:
# ./build.sh FILE [TEMPLATE]

if [ ! -f "${1}" ]; then
    echo "Input file '${1}' doesn't exist!"
    exit 1
fi

# Directory of this script
SCRIPT_DIR="$( cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"

# Directory of the templates
TEMPLATE_DIR="${SCRIPT_DIR}/templates"

# Pandoc template file
TEMPLATE_FILE="${TEMPLATE_DIR}/document.tex"

if [ -n "${2}" ]; then
    TEMPLATE_FILE="${TEMPLATE_DIR}/${2}.tex"
fi

echo "Using template '${TEMPLATE_FILE}'"

# CSL file for bibtex
CSL_FILE="${SCRIPT_DIR}/custom.csl"

# Directory where this script was executed.
# This is where the output will be saved.
START_DIR=$(pwd)

# Directory of the file
FILEDIR=$(dirname "${1}")

# Name of the input file
FILENAME=$(basename "${1}")

# The output file name
OUTPUT_FILENAME=$(echo "${FILENAME}" | rev | cut -d "." -f 2- | rev)

# The output extension
OUTPUT_EXTENSION=".pdf"

# The current date in ISO format
CURRENT_DATE="$(date +%Y-%m-%d)"

# Enter the directory of the file to prevent issues with images
cd "${FILEDIR}" || exit 1

TEMPLATE_ARGUMENTS=()

if [ "${2}" == "beamer" ]; then
    TEMPLATE_ARGUMENTS+=(-t beamer)
    TEMPLATE_ARGUMENTS+=(--slide-level 2)
fi

if [ "${2}" == "cheatsheet" ]; then
    TEMPLATE_ARGUMENTS+=(-V image-width:"0.75")
fi

# Convert the file using pandoc
pandoc \
--citeproc \
--csl="${CSL_FILE}" \
--from markdown+table_captions+backtick_code_blocks+footnotes+inline_notes \
--toc \
--number-sections \
--template "${TEMPLATE_FILE}" \
"${TEMPLATE_ARGUMENTS[@]}" \
--pdf-engine=xelatex \
--pdf-engine-opt=-shell-escape \
"${FILENAME}" \
-o "${START_DIR}/${OUTPUT_FILENAME}${OUTPUT_EXTENSION}" \
-V toc-own-page \
-V titlepage \
-V date:"${CURRENT_DATE}"

RETURN_CODE="$?"

# Return to the start directory
cd "${START_DIR}" || exit 1

echo "Finished building"

exit "$RETURN_CODE"

---
from:
    name: "Severin Kaderli"
    street: "Teststrasse 1"
    zip_code: "1111"
    city: "Teststadt"
to:
    company: "Firma"
    company2: "Department"
    street: "Teststrasse 2"
    zip_code: "1111"
    city: "Teststadt"
location: "Teststadt"
letter_date: "1. Januar 2019"
title: "Hello World"
attachment:
    - Kopie Rechnung
    - Lebenslauf
---
Sehr geehrte Damen und Herren

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
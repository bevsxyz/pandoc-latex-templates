---
title: "Pandoc \\LaTeX\\ Template"
subtitle: "Cheatsheet Example"
author: "Severin Kaderli"
---

# Section
## Subsection
Lorem ipsum dolor sit amet.

## Second Subsection
Lorem ipsum dolor sit amet.

# Second Section
Hello World!
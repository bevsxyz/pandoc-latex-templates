% AUTHOR:
% Severin Kaderli <severin.kaderli@gmail.com>
%
% DESCRIPTION:
% Pandoc LaTeX template for general use in documents.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[
    $if(fontsize)$
        $fontsize$,
    $else$
        12pt,
    $endif$
    $if(papersize)$
        $papersize$,
    $else$
        a4paper,
    $endif$
    oneside,
    fleqn,
    $for(classoption)$
        $classoption$,
    $endfor$
]{article}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[dvipsnames,svgnames*,table]{xcolor}
\usepackage{forest}

$if(glossary)$
    \usepackage[numberedsection, nogroupskip]{glossaries}
    \usepackage{glossaries-extra}
    \setglossarystyle{altlistgroup}
    \makenoidxglossaries
    \input{$glossary-file$}
    \glsaddall
$endif$

\usepackage{algorithm2e}
\usepackage{csquotes}
\usepackage{physics}
\usepackage{hyperref}
\usepackage{ifluatex}
\usepackage{ifxetex}
\usepackage{mdframed}
\usepackage{setspace}
\usepackage{titling}
\usepackage{tikz}
\usepackage[european,RPvoltages]{circuitikz}
\usepackage{environ}

\usepackage{pgfplots}
\pgfplotsset{compat=1.16}

${ _typography() }


\newlength{\cslhangindent}       % set up new length
\setlength{\cslhangindent}{$if(csl-hanging-indent)$2em$else$0em$endif$}
\newenvironment{cslreferences}%
  {\everypar{\setlength{\hangindent}{\cslhangindent}}}%
  {\par} % by default, this env does not change anything


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Geometry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
    margin=2.5cm,
    includehead=true,
    includefoot=true,
    centering,
    driver=xetex,
    $for(geometry)$
        $geometry$,
    $endfor$
]{geometry} 

% Fix for this issue: https://github.com/jgm/pandoc/issues/5841
\renewcommand{\linethickness}{0.4pt}

${ _color() }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multicolumns
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{multicol}
\newcommand{\colbegin[1]}{\begin{multicols}{#1}}
\newcommand{\colend}{\end{multicols}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Put sections on a new page
\let\oldsection\section
\renewcommand\section{\clearpage\oldsection}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paragraph settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}
\makeatletter
\renewcommand{\paragraph}{\@startsection{paragraph}{4}{0ex}%
    {-3.25ex plus -1ex minus -0.2ex}%
    {1.5ex plus 0.2ex}%
    {\normalfont\normalsize\bfseries}}
\makeatother


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Footnote settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(verbatim-in-note)$
    \usepackage{fancyvrb}
    \VerbatimFootnotes
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PDF settings
\PassOptionsToPackage{unicode=true}{hyperref}
\hypersetup{
    colorlinks=true,
    urlcolor=default-urlcolor,
    linkcolor=default-linkcolor,
    filecolor=default-filecolor,
    citecolor=default-citecolor
}

% Use same font for URLs
\urlstyle{same}  

% Forcing linebreaks in URLs 
\PassOptionsToPackage{hyphens}{url}

% Make links footnotes instead of hotlinks:
$if(links-as-notes)$
    \DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$


${ _code() }
${ _list() }
${ _table() }
${ _image() }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Formatting settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(strikeout)$
    \usepackage[normalem]{ulem}
    \pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$

\providecommand{\tightlist}{
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}
}

$if(pagestyle)$
    \pagestyle{$pagestyle$}
$endif$




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section numbering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(numbersections)$
    \setcounter{secnumdepth}{3}
$else$
    \setcounter{secnumdepth}{0}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header and footer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(disable-header-and-footer)$
$else$
\usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead{}
    \fancyfoot{}
    \lhead[
        $if(header-right)$
            $header-right$
        $else$
            $date$
        $endif$
    ]{
        $if(header-left)$
            $header-left$
        $else$
            $title$
            $if(subtitle)$
                \linebreak
                $subtitle$
            $endif$
        $endif$
    }

    \chead[
        $if(header-center)$
            $header-center$
        $endif$
    ]{
        $if(header-center)$
            $header-center$
        $endif$
    }
    
    \rhead[
        $if(header-left)$
            $header-left$
        $else$
            $title$
        $endif$
    ]{
        $if(header-right)$
            $header-right$
        $else$
            $date$
        $endif$
    }

    \lfoot[
        $if(footer-right)$
            $footer-right$
        $else$
            \thepage
        $endif$
    ]{
        $if(footer-left)$
            $footer-left$
        $else$
            $for(author)$
                $author$$sep$,
            $endfor$
        $endif$
    }
    \cfoot[
        $if(footer-center)$
            $footer-center$
        $endif$
    ]{
        $if(footer-center)$
            $footer-center$
        $endif$
    }
    \rfoot[
        $if(footer-left)$
            $footer-left$
        $else$
            $for(author)$
                $author$$sep$,
            $endfor$
        $endif$
    ]{
        $if(footer-right)$
            $footer-right$
        $else$
            \thepage
        $endif$
    }
    
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}
    \setlength{\headheight}{28pt}
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Notes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{tcolorbox}
\newenvironment{note}[1]
{
    \begin{tcolorbox}[frame empty,boxrule=0pt,colbacktitle=note-title-background,colback=note-background,left=2mm,arc=0.75mm,title=\textbf{#1}]
}
{
    \end{tcolorbox}
} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Captions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
    font={stretch=1.2},
    position=top,
    skip=4mm,
    belowskip=4mm,
    labelfont=bf,
    singlelinecheck=false,
    $if(caption-justification)$
        justification=$caption-justification$
    $else$
        justification=raggedright
    $endif$
]{caption}
%\setcapindent{0em}
\captionsetup[longtable]{position=above}


${ _math() }



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Language
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(lang)$
    \usepackage[
        shorthands=off,
        $for(babel-otherlangs)$
            $babel-otherlangs$,
        $endfor$
        main=$babel-lang$
    ]{babel}
$else$
    $if(mainfont)$
    $else$
        % See issue https://github.com/reutenauer/polyglossia/issues/127
        \renewcommand*\familydefault{\sfdefault}
    $endif$
    \usepackage[english]{babel}
$endif$

$if(dir)$
    \usepackage{bidi}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Meta variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{$title$}
\author{
    $for(author)$
        $author$,
    $endfor$
}
\date{$date$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header includes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$for(header-includes)$
    $header-includes$
$endfor$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Font settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{fontspec}
\usepackage{lmodern}
\renewcommand{\familydefault}{\sfdefault}

$if(linestretch)$
    \setstretch{$linestretch$}
$else$
    \setstretch{1.2}
$endif$

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Titlepage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(titlepage)$
    \pagenumbering{gobble}
    \begin{titlepage}
        \newgeometry{left=6cm}

        \newcommand{\colorRule}[3][black]{\textcolor[HTML]{#1}{\rule{#2}{#3}}}

        \begin{flushleft}
            % Set line spacing for title page
            \setstretch{1.4}

            % Text color for title page
            \color[HTML]{4F4F4F}

            % Create ruler on title page
            $if(rule-color)$
                \makebox[0pt][l]{\colorRule[$rule-color$]{1.3\textwidth}{4pt}}
            $else$
                \makebox[0pt][l]{\colorRule[435488]{1.3\textwidth}{4pt}}
            $endif$
            

            \vskip 2em

            % Title
            {\huge \textbf{\textsf{$title$}}}
            
            % Subtitle
            $if(subtitle)$
                {\Large \textsf{$subtitle$}}
            $endif$

            \vfill

            % Authors
            $if(author)$
                \textsf{
                    $for(author)$
                        \phantom{}\hfill $author$\newline
                    $endfor$
                }
            $endif$

            % Extra document information
            $if(extra-info)$
                $if(institute)$
                    \textsf{\hfill $institute$}
                $endif$

                $if(department)$
                    \textsf{\hfill $department$}
                $endif$

                $if(lecturer)$
                    $for(lecturer)$
                        \phantom{}\textsf{\hfill $lecturer$}\newline
                    $endfor$
                $endif$

                \vskip 1em
            $endif$

            % Submission information
            $if(submission)$
                \textsf{\hfill $submission-location$}

                \textsf{\hfill $date$}
            $else$
                \textsf{\hfill $date$}
            $endif$
        \end{flushleft}
    \end{titlepage}
    \restoregeometry
$endif$


${ partials/abstract() }
${ partials/table_of_contents() }

${ body }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Glossary
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(glossary)$
    \printnoidxglossary
$endif$

${ partials/list_of_figures() }
${ partials/list_of_tables() }

\end{document}